from hdfs import InsecureClient
from datetime import datetime

client = InsecureClient(url='http://localhost:9870', root='/')

# 1. Make a directory named: /activity1/
client.makedirs('/activity1/')

# 2.Put the file RandomText.txt into HDFS as the path: /activity1/data/RandomText.txt
client.makedirs('/activity1/data')
client.upload('/activity1/data/RandomText.txt','/RandomText.txt', cleanup=True)
# 3. List the contents of the directory /activity1/data/
client.list('/activity1/data', status=False)
# 4. Move the file /activity1/data/RandomText.txt to /activity1/data/NotSoRandomText.txt
client.rename('/activity1/data/RandomText.txt', '/activity1/data/NotSoRandomText.txt')
# 5. Append the file RandomText.txt to the end of the file: /activity1/data/RandomText.txt
client.write('/activity1/data/RandomText.txt', data=None, overwrite=False, append=True, encoding='utf-8')
# 6. List the disk space used by the directory /activity1/data/
client.status('/activity1/data/',strict=True)
# 7. Put the file MoreRandomText.txt into HDFS as the path: /activity1/data/RandomText.txt
client.upload('/activity1/data/RandomText.txt','MoreRandomText.txt', cleanup=True)
# 8. Recursively list the contents of the directory /activity1/
client.content('/activity1/', strict=True)
# 9. Remove the directory /activity1/ and all files/directories underneath it
client.delete('/activity1/',recursive=True)



