## How much time did you spend on each part of the assignment?Track your time according to the following items: Gitlab & Git, Docker setup/usage, Part 1, and Part 2
- Gitlab & Git: 2 hours
- Docker setup/usage: 20 min
- Part 1: half days
- Part 2: two days
## What was the hardest part of this assignment?
- I think part2 is the hardest part

## What was the easiest part of this assignment?
- Gitlab & Git

## What did I actually learn from doing this assignment?
- I understand that the warehouse created by hadoop is a virtual system, which is different from the system of my own computer. I can store files or run commands in hadoop's virtual warehouse.
## Why does what I learned matter both academically and practically?
- There are many sources of data, and the format of data is becoming more and more complex. As time goes by, the amount of data is also increasing. Therefore, traditional databases quickly become bottlenecks in data storage and data-based calculations. And Hadoop was born to solve such problems. The underlying distributed file system has high scalability, and data redundancy is used to ensure that data is not lost and submit computing efficiency, and it can store data in various formats. At the same time, it also supports a variety of computing frameworks, which can perform offline calculations and online real-time calculations.

